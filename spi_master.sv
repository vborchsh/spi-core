
//
// Project       : SPI core
// Author        : Borchsh Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : spi_master.sv
// Description   : SPI master module
//

`ifndef __SPI_MASTER__
`define __SPI_MASTER__

module spi_master
  #(
    parameter int pW   = 32, // One packet size
    parameter int pDIV = 32  // Divider iclk -> osclk
  )
  (
    input                     iclk   ,
    input                     irst   ,
    // SPI PHY
    output logic              osclk  = '0,
    output logic              omosi  = '0,
    output logic              ocs_n  = '1,
    input                     imiso  ,
    // Data interface
    input                     isop   ,
    input            [pW-1:0] idat   ,
    output logic              osop   = '0, // start of packet
    output logic              oeob   = '0, // end of one byte signal
    output logic              oeop   = '0, // end of packet
    output logic     [pW-1:0] odat   = '0    
  );

  localparam int pINT_DIV = pDIV/2; // For simplification of logic

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------

  logic                          sclk;
  logic                          clk_ena;
  logic                          dat_ena;
  logic                          rx_dat_ena;
  logic                          cs_n;
  logic                    [2:0] fmiso;
  logic                          true_miso;

  logic   [$clog2(pINT_DIV)+1:0] cnt_ena;
  logic         [$clog2(pW)+1:0] cnt_dat;
  logic                 [pW-1:0] rx_shift_reg = '0;
  logic                 [pW-1:0] tx_shift_reg = '0;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------
  
  // Debouncing & metastable protect
  always_ff@(posedge iclk) begin
    fmiso     <= fmiso << 1 | imiso;
    true_miso <= &fmiso[2:1];
  end
  
  // Enable signals
  always_ff@(posedge iclk) begin
    if (irst)                          cnt_ena <= '0;
    else if (cnt_ena == pINT_DIV-1)    cnt_ena <= '0;
      else if (cs_n)                   cnt_ena <= '0;
        else                           cnt_ena <= cnt_ena + 1'b1;

    clk_ena    <=         (cnt_ena == pINT_DIV  -1);
    dat_ena    <= ~sclk & (cnt_ena == pINT_DIV/2-1);
    rx_dat_ena <=  sclk & (cnt_ena == pINT_DIV/2-1);

    if (~cs_n & clk_ena)             sclk <= sclk + 1'b1;
    else if (cs_n)                   sclk <= '0;

    if (irst)                        cs_n <= 1'b1;
    else if (isop)                   cs_n <= 1'b0;
      else if (oeop)                 cs_n <= 1'b1;
  end

  // Data handler
  always_ff@(posedge iclk) begin
    if (irst)                        cnt_dat <= '0;
    else if (cs_n)                   cnt_dat <= '0;
      else if (dat_ena & ~&cnt_dat)  cnt_dat <= cnt_dat + 1'b1;

    if (irst | isop)                 tx_shift_reg <= idat;
    else if (dat_ena)                tx_shift_reg <= tx_shift_reg << 1 | 1'b0;
    // Collect response from slave
    if (irst | isop)                 rx_shift_reg <= '0;
    else if (rx_dat_ena)             rx_shift_reg <= rx_shift_reg << 1 | true_miso;
  end

  //--------------------------------------------------------------------------------------------------
  // Output signals
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    osop  <= dat_ena & (cnt_dat == 'd0);
    odat  <= rx_shift_reg;
    oeop  <= dat_ena & (cnt_dat == pW);
    oeob  <= dat_ena & (cnt_dat%8 == 'd0);
    omosi <= (dat_ena) ? (tx_shift_reg[$high(tx_shift_reg)]) : (omosi);
    ocs_n <= cs_n;
    osclk <= sclk;
  end

endmodule

`endif
